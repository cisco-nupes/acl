#!/bin/bash

USER_NAME="$1"

echo "Username is $USER_NAME"

if [ ! -d output/$USER_NAME ]; then
  mkdir -p output/$USER_NAME;
fi

CREATED="false"

echo "checking policy update file"

cat policy-updates

for POLICY in $(cat policy-updates);
do

    if test -f "$POLICY"; 
    then
        OUTPUT=`aclgen -o --shade_check --debug --definitions_directory definitions --output_directory output/$USER_NAME --policy_file $POLICY`;
        CREATED="true"
    fi
done

if [[ $CREATED = 'false' ]];
    then echo 'No New Policies To Process....exiting pipline';
    exit 1
else
    exit 0;
    echo "New Policies processed. New ACLs created for user $USER_NAME"
fi


