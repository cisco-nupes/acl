#!/bin/bash

# Check if we have modifications to commit
CHANGES=$(git status --porcelain | wc -l)

if ["$CHANGES" -gt "0"]
    then
    # Show the status of files that are about to be created, updated or deleted
    git status

    # Commit all changes
    git commit -m "${COMMIT_MESSAGE}"

    # Update the repository and make sure to skip the pipeline create for this commit
    git push origin "${CI_DEFAULT_BRANCH}" -o ci.skip
fi

